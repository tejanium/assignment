name := "assignment"
version := "1.0"
scalaVersion := "2.11.7"

libraryDependencies += "net.databinder.dispatch" %% "dispatch-core" % "0.11.2"
libraryDependencies += "io.argonaut" %% "argonaut" % "6.0.4"
libraryDependencies += "com.restfb" % "restfb" % "1.18.1"
