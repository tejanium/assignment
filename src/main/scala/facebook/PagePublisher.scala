package facebook

import com.restfb.types.FacebookType
import com.restfb.{DefaultFacebookClient, Parameter}

/**
  * Created by teja on 1/28/16.
  */
class PagePublisher {
  val ACCESS_TOKEN = "CAACEdEose0cBAIFHeGlnFKvr3DsBvcxKegSbLDxIMgI6mUrv0C5EXiZAg84gwybJZBly5pjECRrXkToj908cLsjZAxW02ZAvOaZAKZAvZAKLCdn6We2ETSrBriZCKSbgqBhccUcIcLpIZASkhjj8uoT2afeZCQhZBkEScuJBq4TkYwy834M5Ja9Xn0WbBUVo4pW8bxMBJt8pS6UChjownka5Q3bd9T2oar6raYZD"
  val PAGE_ID = "958521567572809"

  def publish(message: String): FacebookType = {
    val fbClient: DefaultFacebookClient = new DefaultFacebookClient(ACCESS_TOKEN)

    fbClient.publish("546349135390552/feed", classOf[FacebookType],
      Parameter.`with`("message", message)
    )
  }
}

