package models

/**
  * Created by teja on 1/26/16.
  */
case class Movie(
                  title: String,
                  overview: String
                )
