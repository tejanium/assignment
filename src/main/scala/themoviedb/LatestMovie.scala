package themoviedb

/**
  * Created by teja on 1/26/16.
  */

import argonaut.Argonaut._
import argonaut._
import dispatch.Defaults._
import dispatch._
import facebook.PagePublisher
import models.Movie

import scala.util.{Failure, Success}

class LatestMovie extends Base {
  val Path = "movie/latest"
  val URL = "http://" + HOST + "/" + APIVersion + "/" + Path

  def getAndPost() {
    val svc = url(URL + "?api_key=" + APIKey)
    svc.addParameter("api_key", APIKey)

    val response: Future[String] = Http(svc OK as.String)

    response onComplete {
      case Success(content) =>
        val movie: Option[Movie] = parse(content)
        val movieData = movie.get

        val publisher = new PagePublisher
        publisher.publish(movieData.overview)

      case Failure(t) =>
        println("An error has occurred: " + t.getMessage)
    }
  }

  def parse(data: String): Option[Movie] = {
    Parse.decodeOption[Movie](data)
  }

  implicit def UserCodecJson: CodecJson[Movie] =
    casecodec2(Movie.apply, Movie.unapply)("title", "overview")
}
